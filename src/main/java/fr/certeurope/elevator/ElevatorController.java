package fr.certeurope.elevator;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ElevatorController {

    private final ElevatorService elevatorService;

    public ElevatorController(ElevatorService elevatorService) {
        this.elevatorService = elevatorService;
    }

    @PostMapping("/generateJourney")
    public String generateJourney(@RequestBody String input) throws JsonProcessingException {
        return elevatorService.generateElevatorJourney(input);
    }

}
